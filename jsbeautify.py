#!/usr/env/bin python
''' Beautifies the js code to adhere to the styling rules '''

import sys
import jsbeautifier

OPTS = jsbeautifier.default_options()
OPTS.indent_size = 4
OPTS.brace_style = 'end-expand'
OPTS.indent_char = ' '
OPTS.keep_array_indentation = True
OPTS.wrap_line_length = 120
OPTS.end_with_newline = True

if len(sys.argv) > 1:
    BEAUTIFIED = jsbeautifier.beautify_file(sys.argv[1], OPTS)
    with open(sys.argv[1], 'w') as target:
        target.write(BEAUTIFIED)
